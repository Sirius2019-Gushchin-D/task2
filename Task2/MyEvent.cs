using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Task2.Properties;

//using System.Text.RegularExpressions;

namespace Task2
{
    public class MyEvent : INotifyPropertyChanged
    {
        public MyEvent(string name, DateTime start, DateTime end)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            if (start >= end) throw new ArgumentException();
            Start = start;
            End   = end;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public DateTime Start
        {
            get => _start;
            set
            {
                _start = value;
                OnPropertyChanged(nameof(Start));
            }
        }

        public DateTime End
        {
            get => _end;
            set
            {
                _end = value;
                OnPropertyChanged(nameof(End));
            }
        }

        private string   _name;
        private DateTime _start;
        private DateTime _end;
        public  TimeSpan Duration => End - Start;

        public DateTime StartDate
        {
            get => Start;
            set => Start = Start
                          .AddYears(value.Year   - Start.Year)
                          .AddMonths(value.Month - Start.Month)
                          .AddDays(value.Day     - Start.Day);
        }

        public DateTime StartTime
        {
            get => Start;
            set => Start = Start.AddMinutes(value.Minute - Start.Minute).AddHours(value.Hour - Start.Hour);
        }

        public DateTime EndDate
        {
            get => End;
            set => End = End
                        .AddYears(value.Year   - End.Year)
                        .AddMonths(value.Month - End.Month)
                        .AddDays(value.Day     - End.Day);
        }

        public DateTime EndTime
        {
            get => End;
            set => End = End.AddMinutes(value.Minute - End.Minute).AddHours(value.Hour - End.Hour);
        }

        #region Unused

        /*public static MyEvent Parse(string text)
        {
            var regexp =
                new Regex(@"^([^-\n]+)-(\d\d:\d\d \d\d.\d\d.\d\d\d\d)-(\d\d:\d\d \d\d.\d\d.\d\d\d\d)$")
                   .Match(text.Trim());
            if (regexp == Match.Empty) throw new Exception();
            return new MyEvent
            {
                Name = regexp.Groups[1].Value,
                Start = new DateTime(Convert.ToInt32(regexp.Groups[2].Value.Split(' ')[1].Split('.')[2]),
                                     Convert.ToInt32(regexp.Groups[2].Value.Split(' ')[1].Split('.')[1]),
                                     Convert.ToInt32(regexp.Groups[2].Value.Split(' ')[1].Split('.')[0]),
                                     Convert.ToInt32(regexp.Groups[2].Value.Split(' ')[0].Split('.')[0]),
                                     Convert.ToInt32(regexp.Groups[2].Value.Split(' ')[0].Split('.')[1]), 0),
                End = new DateTime(Convert.ToInt32(regexp.Groups[3].Value.Split(' ')[1].Split('.')[2]),
                                   Convert.ToInt32(regexp.Groups[3].Value.Split(' ')[1].Split('.')[1]),
                                   Convert.ToInt32(regexp.Groups[3].Value.Split(' ')[1].Split('.')[0]),
                                   Convert.ToInt32(regexp.Groups[3].Value.Split(' ')[0].Split('.')[0]),
                                   Convert.ToInt32(regexp.Groups[3].Value.Split(' ')[0].Split('.')[1]), 0)
            };
        }*/

        #endregion

        public string FormattedStartDateTime => Start.ToString("HH:mm dd.MM.yyyy");
        public string FormattedEndDateTime   => End.ToString("HH:mm dd.MM.yyyy");

        public string FormattedStartDate => Start.ToString("dd.MM.yyyy");
        public string FormattedEndDate   => End.ToString("dd.MM.yyyy");

        public string FormattedStartTime => Start.ToString("HH:mm");
        public string FormattedEndTime   => End.ToString("HH:mm");

        public override string ToString() => $"{Name}-{Start:HH:mm dd.MM.yyyy}-{End:HH:mm dd.MM.yyyy}";

        public bool Overlaps(MyEvent @event) => @event.Start >= Start        && @event.Start <= End        ||
                                                @event.End   >= Start        && @event.End   <= End        ||
                                                Start        >= @event.Start && Start        <= @event.End ||
                                                End          >= @event.Start && End          <= @event.End;
    }
}