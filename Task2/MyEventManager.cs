using System.Collections.Generic;
using System.Linq;

namespace Task2
{
    public static class MyEventManager
    {
        public static List<MyEvent> ManageEvents(List<MyEvent> events)
        {
            var result = new List<MyEvent>();
            events.Sort((event1, event2) => event1.Duration.CompareTo(event2.Duration));
            foreach (var @event in events)
                if (!result.Any(inResult => inResult.Overlaps(@event)))
                    result.Add(@event);

            result.Sort((event1, event2) => event1.Start.CompareTo(event2.Start));
            return result;
        }
    }
}