using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Task2.Properties;

namespace Task2
{
    public sealed class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        private void NotifyAll()
        {
            OnPropertyChanged(nameof(UserEvents));
            OnPropertyChanged(nameof(TimetableEvents));
            OnPropertyChanged(nameof(UserEventsCount));
            OnPropertyChanged(nameof(EventsInTimetableCount));
        }

        public VmCommand DeleteCommand { get; }
        public VmCommand AddCommand    { get; }

        public ObservableCollection<MyEvent> UserEvents      { get; }
        public List<MyEvent>                 TimetableEvents => MyEventManager.ManageEvents(UserEvents.ToList());

        public int UserEventsCount        => UserEvents.Count;
        public int EventsInTimetableCount => TimetableEvents.Count;

        private MyEvent _selectedEvent;

        public ViewModel()
        {
            UserEvents    = new ObservableCollection<MyEvent>();
            DeleteCommand = new VmCommand(_ => RemoveEvent(SelectedEvent));
            AddCommand    = new VmCommand(_ => AddEvent());
        }

        public void AddEvent()
        {
            var @event =
                new MyEvent
                    (
                     $"Событие {new Random().Next(0, 100)}",
                     DateTime.Now,
                     DateTime.Now.AddDays(1)
                    );
            @event.PropertyChanged += (_, __) => NotifyAll();
            UserEvents.Add(@event);
            UserEvents.MySort(myEvent => myEvent.Start);
            SelectedEvent = @event;
            OnPropertyChanged(nameof(SelectedEvent));
            NotifyAll();
        }

        public void RemoveEvent(MyEvent @event)
        {
            @event.PropertyChanged -= (_, __) => NotifyAll();
            UserEvents.RemoveAt(UserEvents.IndexOf(@event));
            NotifyAll();
        }

        public MyEvent SelectedEvent
        {
            get => _selectedEvent;
            set
            {
                _selectedEvent = value;
                OnPropertyChanged(nameof(SelectedEvent));
            }
        }
    }
}